const std = @import("std");
const ecs = @import("ecs/ecs.zig");

pub fn main() !void {
    const ECS: *ecs.Ecs = ecs.init();
    std.debug.print("{d}\n", .{ECS.numEntities});
    ECS.numEntities = ECS.numEntities + 1;
    std.debug.print("{d}\n", .{ECS.numEntities});
    ECS.components[0] = ecs.Component{ .metaComponent = ecs.MetaComponent{ .kind = "meta" } };
    std.debug.print("{s}\n", .{ECS.components[0].metaComponent.kind});
}
