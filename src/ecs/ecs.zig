const std = @import("std");

const ComponentTagged = enum(u7) {
    noComponent,
    metaComponent,
    textComponent,
    selectableComponent,
    characterComponent,
    playerComponent,
    transformComponent,
    positionComponent,
    velocityComponent,
    jumpComponent,
    flyComponent,
    healthComponent,
    animationComponent,
    renderComponent,
    collideComponent,
    imageComponent,
    textureComponent,
    spriteComponent,
};

pub const NoComponent: type = u1;
pub const MetaComponent: type = struct { kind: []const u8 };
pub const TextComponent: type = struct { text: []const u8, font_size: usize, color: [4]u8 };
pub const TransformComponent: type = struct { x: i16, y: i16 };
pub const VelocityComponent: type = struct { val: i16 };
pub const HealthComponent: type = struct { curVal: i16, maxVal: i16 };
pub const AnimationComponent: type = struct { images: []const u8, interval: f32 };
pub const RenderComponent: type = struct {};
pub const CollideComponent: type = struct { kind: []const u8, ignore: []u20 };
pub const ImageComponent: type = struct { kind: []const u8, imagePath: []const u8 };

pub const Component: type = union(ComponentTagged) {
    noComponent: NoComponent,
    metaComponent: MetaComponent,
    textComponent: TextComponent,
    selectableComponent: bool,
    characterComponent: bool,
    playerComponent: bool,
    transformComponent: TransformComponent,
    positionComponent: bool,
    velocityComponent: VelocityComponent,
    jumpComponent: bool,
    flyComponent: bool,
    healthComponent: HealthComponent,
    animationComponent: AnimationComponent,
    renderComponent: RenderComponent,
    collideComponent: CollideComponent,
    imageComponent: ImageComponent,
    textureComponent: bool,
    spriteComponent: bool,
};

const InitMeta: type = struct {
    screen_width: comptime_int,
    screen_height: comptime_int,
    game_engine: type,
    game_engine_id: u2, // 0: raylib, 1: SDL, 2: SFML, 3: GLFW
    game_engine_is_c_based: bool,
};

pub fn init() *Ecs {
    var e = Ecs{
        .meta_data = InitMeta{
            .screen_width = 1920,
            .screen_height = 1080,
            .game_engine = @cImport(@cInclude("/home/overflyer/CodeProjects/ZigProjects/zecs/src/raylib/src/raylib.h")),
            .game_engine_id = 0,
            .game_engine_is_c_based = true,
        },

        .game_engine_sprite_n_texture_are_same = false,
        .numEntities = 0,
        .components = [_]Component{Component{ .noComponent = 0 }} ** 2097152,
    };

    e.game_engine_sprite_n_texture_are_same = if (e.meta_data.game_engine_id == 0) true else false;
    return @constCast(&e);
}

pub fn addEntity(self: *Ecs) u16 {
    self.numEntities += 1;
    return self.numEntities;
}

pub const Ecs: type = struct {
    meta_data: InitMeta,
    numEntities: u16,
    components: [2097152]Component,
    game_engine_sprite_n_texture_are_same: bool,
};
